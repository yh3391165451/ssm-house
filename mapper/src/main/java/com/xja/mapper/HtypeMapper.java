package com.xja.mapper;

import com.xja.entity.Htype;

public interface HtypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Htype record);

    int insertSelective(Htype record);

    Htype selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Htype record);

    int updateByPrimaryKey(Htype record);
}