package com.xja.service;

import com.xja.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

  User login(String name) throws Exception;
}
