package com.xja.service.impl;

import com.xja.entity.User;
import com.xja.mapper.UserMapper;
import com.xja.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service("userService")
public class UserServiceImpl  implements UserService {
    @Autowired
    private UserMapper userMapper;


    @Override
    public User login(String name) throws Exception {
        return userMapper.login(name);
    }
}
