package com.xja.controller;

import com.xja.entity.User;
import com.xja.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("admin")
public class AdminController {
    @Autowired
    private UserService userService;
    @RequestMapping("login")
    public String login(User user, Model model){
        try{
            User login=userService.login(user.getName());
            if (login == null) {
                model.addAttribute("msg","用户名错误");
                return "Login"; }
            if (!login.getPassword().equals(user.getPassword())) {
                model.addAttribute("msg","密码错误");
                return "Login"; }
            return "list";
        }catch (Exception e){
            e.printStackTrace();
        }return "404";
    }
}
