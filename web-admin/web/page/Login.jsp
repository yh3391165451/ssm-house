<%--
  Created by IntelliJ IDEA.
  User: 老师
  Date: 2021/8/10
  Time: 16:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >
<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD><TITLE>爱家租房 - 用户登录</TITLE>
    <META content="text/html; charset=utf-8" http-equiv=Content-Type><LINK
            rel=stylesheet type=text/css href="/css/style.css">
    <META name=GENERATOR content="MSHTML 8.00.7601.17514"></HEAD>
<BODY>
<DIV id=header class=wrap>
    <DIV ><IMG src="/images/logo.gif"></DIV></DIV>
<DIV  class=wrap>
    <DIV class=dialog>
        <DIV class=box>
            <H4>用户登录</H4>
            <form action="/admin/login.do" method="post">
                <DIV class=infos>
                    <TABLE class=field>
                        <TBODY>
                        <TR>
                            <TD colSpan=3></TD></TR>
                        <TR>
                            <TD class=field>用 户 名：</TD>
                            <TD>
                                <input type="text"  name="name" />
<%--                                <INPUT id=user_name class=text type=text name=name>--%>
                            </TD></TR>
                        <TR>
                            <TD class=field>密　　码：</TD>
                            <TD>
                                <input type="password" name="password" />
<%--                                <INPUT id=user_password class=text type=password name=password>--%>
                            </TD></TR>
                        <TR>
                            <TD class=field>提    示：</TD>
                            <TD>
                               <h2>${msg}</h2>       </TD></TR>
                        </TBODY></TABLE>

                    <DIV  class="buttons"> <INPUT value=登陆 type=submit>
                        <INPUT value=注册 type=submit>
                    </DIV></DIV></FORM></DIV></DIV></DIV>
<DIV id=footer class=wrap>
    <DL>
        <DT>爱家租房 © 2010 北大爱家 京ICP证1000001号</DT>
        <DD>关于我们 · 联系方式 · 意见反馈 · 帮助中心</DD></DL></DIV></BODY></HTML>
